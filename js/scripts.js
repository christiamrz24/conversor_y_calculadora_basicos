function convertir(){
    var octal = document.getElementById("n_a_convertir").value;
    if(octal == ""){ //Comprobar que la caja no este vacía
        msg_error(1);
    } else if(isNaN(octal)){ //Comprobar que no se ha introducido un número
        msg_error(2);
    }
    else{ //Realizar y mostrar el resultado de la conversión
        alert("El número " + octal + " convertido a base 10 es: " + parseInt(octal, 8));
    }
}
function calcular(){
    var num1 = document.getElementById("numero_1").value;
    var num2 = document.getElementById("numero_2").value;
    if(num1 == "" || num2 == ""){
        msg_error(1);
    } else if(isNaN(num1) || isNaN(num2)){
        msg_error(2);
    } else{
        num1 = parseInt(num1);
        num2 = parseInt(num2);
        alert(
              "RESULTADOS: " + "\n" +
              " -> Suma: " + (num1 + num2) + "\n" +
              " -> Resta: " + (num1 - num2) + "\n" +
              " -> Multiplicación: " + (num1 * num2) + "\n" +
              " -> División: " + (num1 / num2)
            );
    }
}
function msg_error(id){
    if(id == 1){
        alert("Introduzca un valor en los campos!");
    }
    if(id == 2){
        alert("Error! El dígito introducido no es un número.");
    }
}